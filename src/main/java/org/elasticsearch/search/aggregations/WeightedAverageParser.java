/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 team-unpro
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


package org.elasticsearch.search.aggregations;

import org.elasticsearch.common.ParsingException;
import org.elasticsearch.common.xcontent.XContentParser;
import org.elasticsearch.index.query.QueryParseContext;
import org.elasticsearch.script.Script;

import java.io.IOException;

public class WeightedAverageParser implements Aggregator.Parser {
    @Override
    public AggregationBuilder parse(String aggregationName, QueryParseContext context) throws IOException {

        XContentParser parser = context.parser();
        String valueField = null;
        Script valueScript = null;
        String weightField = null;
        Script weightScript = null;

        XContentParser.Token token;
        String currentFieldName = null;
        while ((token = parser.nextToken()) != XContentParser.Token.END_OBJECT) {
            if (token == XContentParser.Token.FIELD_NAME) {
                currentFieldName = parser.currentName();
            } else if ("value".equals(currentFieldName) && token == XContentParser.Token.START_OBJECT) {
                ScriptOrField value = parseScriptOrField(aggregationName, context);
                valueField = value.field;
                valueScript = value.script;
            } else if ("weight".equals(currentFieldName) && token == XContentParser.Token.START_OBJECT) {
                ScriptOrField weight = parseScriptOrField(aggregationName, context);
                weightField = weight.field;
                weightScript = weight.script;
            } else {
                throw new ParsingException(parser.getTokenLocation(),
                        "Unexpected token " + token + " [" + currentFieldName + "] in [" + aggregationName + "].");
            }
        }
        WeightedAverageAggregationBuilder builder = new WeightedAverageAggregationBuilder(aggregationName);
        if (valueField != null) {
            builder.valueField(valueField);
        }
        if (valueScript != null) {
            builder.valueScript(valueScript);
        }
        if (weightField != null) {
            builder.weightField(weightField);
        }
        if (weightScript != null) {
            builder.weightScript(weightScript);
        }
        return builder;
    }

    private ScriptOrField parseScriptOrField(String aggregationName, QueryParseContext context) throws IOException {
        String field = null;
        Script script = null;
        XContentParser parser = context.parser();
        XContentParser.Token token;
        String currentFieldName = null;
        while ((token = parser.nextToken()) != XContentParser.Token.END_OBJECT) {
            if (token == XContentParser.Token.FIELD_NAME) {
                currentFieldName = parser.currentName();
            } else if (token == XContentParser.Token.VALUE_STRING && "field".equals(currentFieldName)) {
                field = parser.text();
            } else if (token == XContentParser.Token.START_OBJECT &&
                    context.getParseFieldMatcher().match(currentFieldName, Script.SCRIPT_PARSE_FIELD)) {
                script = Script.parse(parser, context.getParseFieldMatcher(), context.getDefaultScriptLanguage());
            } else {
                throw new ParsingException(parser.getTokenLocation(),
                        "Unexpected token " + token + " [" + currentFieldName + "] in [" + aggregationName + "].");

            }
        }
        return new ScriptOrField(field, script);
    }

    private static class ScriptOrField {
        private String field;
        private Script script;

        ScriptOrField(String field, Script script) {
            this.field = field;
            this.script = script;
        }
    }
}
