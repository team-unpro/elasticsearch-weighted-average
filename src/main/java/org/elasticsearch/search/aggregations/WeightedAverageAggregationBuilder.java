/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 team-unpro
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


package org.elasticsearch.search.aggregations;

import org.elasticsearch.common.io.stream.StreamInput;
import org.elasticsearch.common.io.stream.StreamOutput;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.index.fielddata.IndexFieldData;
import org.elasticsearch.index.mapper.MappedFieldType;
import org.elasticsearch.script.Script;
import org.elasticsearch.script.ScriptContext;
import org.elasticsearch.script.SearchScript;
import org.elasticsearch.search.aggregations.support.AggregationContext;
import org.elasticsearch.search.aggregations.support.FieldContext;
import org.elasticsearch.search.aggregations.support.ValueType;
import org.elasticsearch.search.aggregations.support.ValuesSource;
import org.elasticsearch.search.aggregations.support.ValuesSourceConfig;
import org.elasticsearch.search.aggregations.support.ValuesSourceType;
import org.elasticsearch.search.internal.SearchContext;

import java.io.IOException;
import java.util.Objects;

public class WeightedAverageAggregationBuilder extends AbstractAggregationBuilder<WeightedAverageAggregationBuilder> {
    public static final String NAME = "weightedAverage";
    private static final InternalAggregation.Type TYPE = new InternalAggregation.Type(NAME);

    private String valueField = null;
    private Script valueScript = null;
    private String weightField = null;
    private Script weightScript = null;

    public WeightedAverageAggregationBuilder(String name) {
        super(name, TYPE);
    }

    public WeightedAverageAggregationBuilder(StreamInput in) throws IOException {
        super(in, TYPE);
        read(in);
    }

    private void read(StreamInput in) throws IOException {
        valueField = in.readOptionalString();
        if (in.readBoolean()) {
            valueScript = new Script(in);
        }
        weightField = in.readOptionalString();
        if (in.readBoolean()) {
            weightScript = new Script(in);
        }
    }

    @Override
    protected void doWriteTo(StreamOutput out) throws IOException {
        out.writeOptionalString(valueField);
        boolean hasValueScript = valueScript != null;
        out.writeBoolean(hasValueScript);
        if (hasValueScript) {
            valueScript.writeTo(out);
        }
        out.writeOptionalString(weightField);
        boolean hasWeightScript = weightScript != null;
        out.writeBoolean(hasWeightScript);
        if (hasWeightScript) {
            weightScript.writeTo(out);
        }
    }

    @Override
    protected AggregatorFactory<?> doBuild(AggregationContext context, AggregatorFactory<?> parent,
                                           AggregatorFactories.Builder subfactoriesBuilder) throws IOException {
        ValuesSourceConfig<ValuesSource.Numeric> valuesSourceConfig = config(context, valueField, valueScript);
        ValuesSourceConfig<ValuesSource.Numeric> weightsSourceConfig = config(context, weightField, weightScript);
        return new WeightedAverageAggregatorFactory(name, type, valuesSourceConfig, weightsSourceConfig, context, parent,
                subfactoriesBuilder, metaData);
    }

    public ValuesSourceConfig<ValuesSource.Numeric> config(AggregationContext context, String field, Script script) {
        if (field == null) {
            if (script == null) {
                ValuesSourceConfig<ValuesSource.Numeric> config = new ValuesSourceConfig<>(ValuesSourceType.ANY);
                config.format(ValueType.NUMERIC.defaultFormat());
                return config;
            }
            ValuesSourceConfig<ValuesSource.Numeric> config = new ValuesSourceConfig<>(ValuesSourceType.NUMERIC);
            config.format(ValueType.NUMERIC.defaultFormat());
            config.script(createScript(script, context.searchContext()));
            config.scriptValueType(ValueType.NUMERIC);
            return config;
        }

        MappedFieldType fieldType = context.searchContext().smartNameFieldType(field);
        if (fieldType == null) {
            ValuesSourceConfig<ValuesSource.Numeric> config = new ValuesSourceConfig<>(ValuesSourceType.NUMERIC);
            config.format(ValueType.NUMERIC.defaultFormat());
            config.unmapped(true);
            config.scriptValueType(ValueType.NUMERIC);
            return config;
        }

        IndexFieldData<?> indexFieldData = context.searchContext().fieldData().getForField(fieldType);

        ValuesSourceConfig<ValuesSource.Numeric> config = new ValuesSourceConfig<>(ValuesSourceType.NUMERIC);

        config.fieldContext(new FieldContext(field, indexFieldData, fieldType));
        config.script(createScript(script, context.searchContext()));
        config.format(fieldType.docValueFormat(null, null));
        return config;
    }

    private SearchScript createScript(Script script, SearchContext context) {
        if (script == null) {
            return null;
        } else {
            return context.getQueryShardContext().getSearchScript(script, ScriptContext.Standard.AGGS);
        }
    }

    @Override
    protected XContentBuilder internalXContent(XContentBuilder builder, Params params) throws IOException {
        builder.startObject();
        if (valueField != null || valueScript != null) {
            builder.startObject("value");
            if (valueField != null) {
                builder.field("field", valueField);
            }
            if (valueScript != null) {
                builder.field("script", valueScript);
            }
            builder.endObject();
        }
        if (weightField != null || weightScript != null) {
            builder.startObject("weight");
            if (weightField != null) {
                builder.field("field", weightField);
            }
            if (weightScript != null) {
                builder.field("script", weightScript);
            }
            builder.endObject();
        }
        builder.endObject();
        return builder;
    }

    @Override
    protected int doHashCode() {
        return Objects.hash(valueField, valueScript, weightField, weightScript);
    }

    @Override
    protected boolean doEquals(Object obj) {
        WeightedAverageAggregationBuilder other = (WeightedAverageAggregationBuilder) obj;
        if (!Objects.equals(valueField, other.valueField))
            return false;
        if (!Objects.equals(valueScript, other.valueScript))
            return false;
        if (!Objects.equals(weightField, other.weightField))
            return false;
        if (!Objects.equals(weightScript, other.weightScript))
            return false;
        return true;
    }

    @Override
    public String getWriteableName() {
        return NAME;
    }

    @Override
    public WeightedAverageAggregationBuilder subAggregation(AggregationBuilder aggregation) {
        throw new AggregationInitializationException("Aggregator [" + name + "] of type [" + NAME + "] cannot accept sub-aggregations");
    }

    public WeightedAverageAggregationBuilder valueField(String valueField) {
        if (valueField == null) {
            throw new IllegalArgumentException("[valueField] must not be null: [" + name + "]");
        }
        this.valueField = valueField;
        return this;
    }

    public WeightedAverageAggregationBuilder valueScript(Script valueScript) {
        if (valueScript == null) {
            throw new IllegalArgumentException("[valueField] must not be null: [" + name + "]");
        }
        this.valueScript = valueScript;
        return this;
    }

    public WeightedAverageAggregationBuilder weightField(String weightField) {
        if (weightField == null) {
            throw new IllegalArgumentException("[valueField] must not be null: [" + name + "]");
        }
        this.weightField = weightField;
        return this;
    }

    public WeightedAverageAggregationBuilder weightScript(Script weightScript) {
        if (weightScript == null) {
            throw new IllegalArgumentException("[valueField] must not be null: [" + name + "]");
        }
        this.weightScript = weightScript;
        return this;
    }
}
