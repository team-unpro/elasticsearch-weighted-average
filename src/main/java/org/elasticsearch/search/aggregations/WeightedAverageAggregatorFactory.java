/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 team-unpro
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


package org.elasticsearch.search.aggregations;

import org.elasticsearch.search.aggregations.pipeline.PipelineAggregator;
import org.elasticsearch.search.aggregations.support.AggregationContext;
import org.elasticsearch.search.aggregations.support.ValuesSource;
import org.elasticsearch.search.aggregations.support.ValuesSourceConfig;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class WeightedAverageAggregatorFactory extends AggregatorFactory<WeightedAverageAggregatorFactory> {

    private ValuesSourceConfig<ValuesSource.Numeric> valuesSourceConfig;
    private ValuesSourceConfig<ValuesSource.Numeric> weightsSourceConfig;

    public WeightedAverageAggregatorFactory(String name, InternalAggregation.Type type,
                                            ValuesSourceConfig<ValuesSource.Numeric> valuesSourceConfig,
                                            ValuesSourceConfig<ValuesSource.Numeric> weightsSourceConfig,
                                            AggregationContext context, AggregatorFactory<?> parent,
                                            AggregatorFactories.Builder subFactoriesBuilder,
                                            Map<String, Object> metaData) throws IOException {
        super(name, type, context, parent, subFactoriesBuilder, metaData);
        this.valuesSourceConfig = valuesSourceConfig;
        this.weightsSourceConfig = weightsSourceConfig;
    }

    @Override
    protected Aggregator createInternal(Aggregator parent, boolean collectsFromSingleBucket, List<PipelineAggregator> pipelineAggregators,
                                        Map<String, Object> metaData) throws IOException {
        ValuesSource.Numeric vs = context.valuesSource(valuesSourceConfig, context.searchContext());
        ValuesSource.Numeric ws = context.valuesSource(weightsSourceConfig, context.searchContext());
        if (vs == null || ws == null) {
            return createUnmapped(parent, pipelineAggregators, metaData);
        }
        return doCreateInternal(vs, ws, parent, collectsFromSingleBucket, pipelineAggregators, metaData);
    }

    private Aggregator doCreateInternal(ValuesSource.Numeric vs, ValuesSource.Numeric ws, Aggregator parent,
                                        boolean collectsFromSingleBucket, List<PipelineAggregator> pipelineAggregators,
                                        Map<String, Object> metaData) throws IOException {
        return new WeightedAvgerageAggregator(name, vs, ws, context, parent, pipelineAggregators, metaData);
    }

    private Aggregator createUnmapped(Aggregator parent, List<PipelineAggregator> pipelineAggregators,
                                      Map<String, Object> metaData) throws IOException {
        return new WeightedAvgerageAggregator(name, null, null, context, parent, pipelineAggregators, metaData);
    }
}
