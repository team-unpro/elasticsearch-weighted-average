/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 team-unpro
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


package org.elasticsearch.search.aggregations;

import org.elasticsearch.common.io.stream.StreamInput;
import org.elasticsearch.common.io.stream.StreamOutput;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.search.DocValueFormat;
import org.elasticsearch.search.aggregations.metrics.InternalNumericMetricsAggregation;
import org.elasticsearch.search.aggregations.pipeline.PipelineAggregator;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class InternalWeightedAverage extends InternalNumericMetricsAggregation.SingleValue {
    private final double weightedSum;
    private final long totalWeight;

    public InternalWeightedAverage(String name, double weightedSum, long totalWeight, List<PipelineAggregator> pipelineAggregators,
                                   Map<String, Object> metaData) {
        super(name, pipelineAggregators, metaData);
        this.weightedSum = weightedSum;
        this.totalWeight = totalWeight;
    }

    /**
     * Read from a stream.
     */
    public InternalWeightedAverage(StreamInput in) throws IOException {
        super(in);
        weightedSum = in.readDouble();
        totalWeight = in.readLong();
    }

    @Override
    protected void doWriteTo(StreamOutput out) throws IOException {
        out.writeDouble(weightedSum);
        out.writeLong(totalWeight);
    }

    @Override
    public InternalAggregation doReduce(List<InternalAggregation> aggregations, ReduceContext reduceContext) {
        long totalWeight = 0;
        double weightedSum = 0;
        for (InternalAggregation aggregation : aggregations) {
            totalWeight += ((InternalWeightedAverage) aggregation).totalWeight;
            weightedSum += ((InternalWeightedAverage) aggregation).weightedSum;
        }
        return new InternalWeightedAverage(getName(), weightedSum, totalWeight, pipelineAggregators(), getMetaData());
    }

    @Override
    public XContentBuilder doXContentBody(XContentBuilder builder, Params params) throws IOException {
        builder.field(CommonFields.VALUE, totalWeight != 0 ? value() : null);
        if (totalWeight != 0 && format != DocValueFormat.RAW) {
            builder.field(CommonFields.VALUE_AS_STRING, format.format(value()));
        }
        return builder;
    }

    @Override
    public String getWriteableName() {
        return WeightedAverageAggregationBuilder.NAME;
    }

    @Override
    public double value() {
        return weightedSum / totalWeight;
    }

    public double getValue() {
        return value();
    }
}
