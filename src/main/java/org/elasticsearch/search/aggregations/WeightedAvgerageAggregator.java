/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 team-unpro
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


package org.elasticsearch.search.aggregations;

import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.index.SortedNumericDocValues;
import org.elasticsearch.common.lease.Releasables;
import org.elasticsearch.common.util.BigArrays;
import org.elasticsearch.common.util.DoubleArray;
import org.elasticsearch.common.util.LongArray;
import org.elasticsearch.index.fielddata.SortedNumericDoubleValues;
import org.elasticsearch.search.aggregations.metrics.NumericMetricsAggregator;
import org.elasticsearch.search.aggregations.pipeline.PipelineAggregator;
import org.elasticsearch.search.aggregations.support.AggregationContext;
import org.elasticsearch.search.aggregations.support.ValuesSource;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class WeightedAvgerageAggregator extends NumericMetricsAggregator.SingleValue {
    private ValuesSource.Numeric valuesSource;
    private ValuesSource.Numeric weightsSource;

    LongArray totalWeights;
    DoubleArray weightedSums;

    protected WeightedAvgerageAggregator(String name, ValuesSource.Numeric valuesSource, ValuesSource.Numeric weightsSource,
                                         AggregationContext context, Aggregator parent,
                                         List<PipelineAggregator> pipelineAggregators, Map<String, Object> metaData) throws IOException {
        super(name, context, parent, pipelineAggregators, metaData);
        this.valuesSource = valuesSource;
        this.weightsSource = weightsSource;
        final BigArrays bigArrays = context.bigArrays();
        if (valuesSource != null) {
            weightedSums = bigArrays.newDoubleArray(1, true);
            totalWeights = bigArrays.newLongArray(1, true);
        }
    }

    @Override
    protected LeafBucketCollector getLeafCollector(LeafReaderContext ctx, LeafBucketCollector sub) throws IOException {
        if (valuesSource == null || weightsSource == null) {
            return LeafBucketCollector.NO_OP_COLLECTOR;
        }
        final BigArrays bigArrays = context.bigArrays();
        final SortedNumericDoubleValues values = valuesSource.doubleValues(ctx);
        final SortedNumericDocValues weights = weightsSource.longValues(ctx);
        return new LeafBucketCollectorBase(sub, values) {
            @Override
            public void collect(int doc, long bucket) throws IOException {
                totalWeights = bigArrays.grow(totalWeights, bucket + 1);
                weightedSums = bigArrays.grow(weightedSums, bucket + 1);

                values.setDocument(doc);
                weights.setDocument(doc);
                if (values.count() != weights.count()) {
                    throw new AggregationExecutionException("The values count does not equal the weight count: " +
                            values.count() + " != " + weights.count());
                }
                double weightedSum = 0;
                long totalWeight = 0;
                for (int i = 0; i < values.count(); i++) {
                    long weight = weights.valueAt(i);
                    weightedSum += values.valueAt(i) * weight;
                    totalWeight += weight;
                }
                totalWeights.increment(bucket, totalWeight);
                weightedSums.increment(bucket, weightedSum);
            }
        };
    }

    @Override
    public InternalAggregation buildAggregation(long bucket) throws IOException {
        if (valuesSource == null || weightsSource == null || bucket >= weightedSums.size()) {
            return buildEmptyAggregation();
        }
        return new InternalWeightedAverage(name, weightedSums.get(bucket), totalWeights.get(bucket), pipelineAggregators(), metaData());
    }

    @Override
    public InternalAggregation buildEmptyAggregation() {
        return new InternalWeightedAverage(name, 0.0, 0L, pipelineAggregators(), metaData());
    }

    @Override
    public double metric(long owningBucketOrd) {
        if (valuesSource == null || weightsSource == null || owningBucketOrd >= weightedSums.size()) {
            return Double.NaN;
        }
        return weightedSums.get(owningBucketOrd) / totalWeights.get(owningBucketOrd);
    }

    @Override
    protected void doClose() {
        Releasables.close(totalWeights, weightedSums);
    }
}
